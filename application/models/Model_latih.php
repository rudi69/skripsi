<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_latih extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    var $table = 'sa_datauji';
    var $column_order = array('text','sentimen_eucledian','sentimen_cosine','sentimen_jaccord',null); //set column field database for datatable orderable
    var $column_search = array('text','sentimen_eucledian','sentimen_cosine','sentimen_jaccord'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id_datauji' => 'asc'); // default order 
    var $join = array('sa_dataset'=> 'id_dataset2=id_dataset');

    private function _get_datatables_query()
    {

        $this->db->from($this->table);
		foreach ($this->join as $table => $kondisi) {
            $this->db->join($table,$kondisi);
        }
        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_term',$id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id_term', $id);
        $this->db->delete($this->table);
    }
	public function all_terms_testdata(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA UJI');
		$array_terms = $this->db->get()->result_array();
		$array_terms= array_column($array_terms,'term_stemmed');
		$all_terms = implode(" ",$array_terms);
		$all_terms = preg_replace('/\s+/', ' ', $all_terms);
		$all_terms = trim($all_terms);
		$array_terms = explode(" ",$all_terms);
		return $array_terms;
	}
	//ambil semua term dari semua data latih
	public function all_terms_traindata(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA LATIH');
		$array_terms = $this->db->get()->result_array();
		$array_terms= array_column($array_terms,'term_stemmed');
		$all_terms = implode(" ",$array_terms);
		$all_terms = preg_replace('/\s+/', ' ', $all_terms);
		$all_terms = trim($all_terms);
		$array_terms = explode(" ",$all_terms);
		return $array_terms;
	}
	public function all_terms_traindata_positif(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA LATIH');
		$this->db->where('jenis_sentimen','POSITIF');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
	}
	public function all_terms_traindata_negatif(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA LATIH');
		$this->db->where('jenis_sentimen','NEGATIF');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
	}

	//ambil array semua term dari data latih positif
	public function array_pos_terms(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('sa_dataset.jenis_sentimen','POSITIF');
		$this->db->where('sa_dataset.kategori','DATA LATIH');
		$array_pos_terms = $this->db->get()->result_array();
		$array_pos_terms= array_column($array_pos_terms,'term_stemmed');
		$all_pos_terms = implode(" ",$array_pos_terms);
		$all_pos_terms = preg_replace('/\s+/', ' ', $all_pos_terms);
		$all_pos_terms = trim($all_pos_terms);
		$array_pos_terms = explode(" ",$all_pos_terms);
		//die(var_dump($array_pos_terms));
		return $array_pos_terms;
	}

	//ambil array semua term dari data latih negatif
	public function array_neg_terms(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('sa_dataset.jenis_sentimen','NEGATIF');
		$this->db->where('sa_dataset.kategori','DATA LATIH');
		$array_neg_terms = $this->db->get()->result_array();
		$array_neg_terms= array_column($array_neg_terms,'term_stemmed');
		$all_neg_terms = implode(" ",$array_neg_terms);
		$all_neg_terms = preg_replace('/\s+/', ' ', $all_neg_terms);
		$all_neg_terms = trim($all_neg_terms);
		$array_neg_terms = explode(" ",$all_neg_terms);
		return $array_neg_terms;
	}
	
	//ambil array semua term yang unik dari semua data latih (vocabulary)
	public function vocabulary(){
		$array_terms = $this->all_terms_traindata();
		$vocabulary = array_unique($array_terms);
		$array_vocabulary = array();
		foreach ($vocabulary as $unique_term) {
			array_push($array_vocabulary,$unique_term);
		}
		return $array_vocabulary;
	}
	
	/*---------------------------PROSES TESTING---------------------------*/
	
	//hitung total review di data uji
	public function count_total_testdata(){
		$this->db->select('id_dataset');
		$this->db->from('sa_dataset');
		$this->db->where('kategori','DATA UJI');
		$total_testdata = $this->db->count_all_results();
		return $total_testdata;
	}
	
	//ambil semua review data uji
	public function all_test_docs(){
		$this->db->select('sa_dataset.id_dataset, sa_bagofwords.term_stemmed');
		$this->db->from('sa_dataset');
		
		$this->db->join('sa_bagofwords', 'sa_bagofwords.bag_id_dataset = sa_dataset.id_dataset');
		//$this->db->where('kategori','DATA UJI');
		$array_test_docs = $this->db->get()->result_array();
		return $array_test_docs;
	}
	public function all_test_docs2(){
		$this->db->select('sa_dataset.id_dataset, sa_dataset.text');
		$this->db->from('sa_dataset');
		$this->db->where('kategori','DATA UJI');
		//$this->db->join('sa_bagofwords', 'sa_bagofwords.bag_id_dataset = sa_dataset.id_dataset');
		
		$array_test_docs = $this->db->get()->result_array();
		return $array_test_docs;
	}
	
	//ambil semua isi tabel vocabulary (frekuensi kemunculan term dan likelihood)
	public function all_vocabs(){
		$this->db->select('*');
		$this->db->from('sa_vocabulary');
		$array_all_vocabs = $this->db->get()->result_array();
		return $array_all_vocabs;
	}
	public function insert_datauji($k){
		$this->db->truncate('sa_datauji');
		$array_uji= $this->model_latih->get_all_data_uji();
		$id_data_uji=$this->model_latih->get_all_id_data_uji();
		$data_model=$this->tfidficf_latih();
		$i=0;
        foreach ($array_uji as $row_uji) {
			$hasil_prediksi=NULL;
			$data = $this->tfidficf($row_uji,$k,$data_model);
			$hasil_prediksi[]=array(
				"id_dataset2" => $id_data_uji[$i]["bag_id_dataset"],
				"sentimen_eucledian" => $data[0]["sentimen_eucledian"],
				"sentimen_cosine" => $data[0]["sentimen_cosine"],
				"sentimen_jaccord" => $data[0]["sentimen_jaccord"]
			);
			//die(var_dump($hasil_prediksi));
			$this->db->insert_batch('sa_datauji',$hasil_prediksi);
			$i++;
		}
	}
	public function insert_tfidf($k){
		$this->db->truncate('sa_datauji');
		$array_uji= $this->model_latih->get_all_data_uji();
		$id_data_uji=$this->model_latih->get_all_id_data_uji();
		$data_model=$this->tfidficf_latih();
		var_dump();
		$i=0;
        foreach ($array_uji as $row_uji) {
			$hasil_prediksi=NULL;
			$data = $this->tfidf($row_uji,$k,$data_model);
			$hasil_prediksi[]=array(
				"id_dataset2" => $id_data_uji[$i]["bag_id_dataset"],
				"sentimen_eucledian" => $data[0]["sentimen_eucledian"],
				"sentimen_cosine" => $data[0]["sentimen_cosine"],
				"sentimen_jaccord" => $data[0]["sentimen_jaccord"]
			);
			//die(var_dump($hasil_prediksi));
			$this->db->insert_batch('sa_datauji',$hasil_prediksi);
			$i++;
		}
	}
	
	//isi badge di tabel
	public function accuracy_badge($predicted,$result){
		$badge="<span class='badge bg-gray'>BLM DIKETAHUI</span>";
		if(isset($result)){
			if($predicted==$result){
				$badge="<span class='badge bg-green'>AKURAT</span>";
			}else{
				$badge="<span class='badge bg-red'>TDK AKURAT</span>";
			}
		}
		return $badge;
	}
	
	//ambil sentimen asli dan sentimen hasil analisis
	public function get_sentiments(){
		$this->db->select('sa_dataset.jenis_sentimen, sa_datauji.sentimen_datauji');
		$this->db->from('sa_dataset');
		$this->db->where('kategori','DATA UJI');
		$this->db->join('sa_datauji', 'sa_datauji.id_dataset2 = sa_dataset.id_dataset');
		$array_all_sentiments = $this->db->get()->result_array();
		return $array_all_sentiments;
	}
	
	public function matrix_akurasi(){
		$array_sentiments = $this->get_sentiments();
		$total_datauji =  count($array_sentiments);
		$true_positives =0;
		$true_negatives =0;
		$false_positives =0;
		$false_negatives =0;
		
		foreach($array_sentiments as $sentiment){
			if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_datauji"]=="POSITIF"){
				$true_positives = $true_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_datauji"]=="NEGATIF"){
				$true_negatives = $true_negatives+1;
			}
			else if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_datauji"]=="NEGATIF"){
				$false_positives = $false_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_datauji"]=="POSITIF"){
				$false_negatives = $false_negatives+1;
			}
		}
		
		$akurasi = ($true_positives+$true_negatives)/$total_datauji; //AKURASI :(true positives+true negatives)/total data uji
		$error_rate = 1- $akurasi; //ERROR-RATE : 1 - akurasi (tingkat kesalahan sistem)
		$ppv = $true_positives/($true_positives+$false_positives); //POSITIVE PREDICTION VALUE /PRESISI : true positives/(true positives+false positives)
		$npv = $true_negatives/($true_negatives+$false_negatives); //NEGATIVE PREDICTION VALUE : true negatives/(true negatives+false negatives)
		$sensitivity = $true_positives/($true_positives+$false_negatives); //SENSITIVITY /RECALL : true positives/(true positives+false negatives)
		$specificity = $true_negatives/($true_negatives+$false_positives); //SPECIFICITY : true negatives/(true negatives+false positives)
		$array_data_matriks = array($total_datauji, $true_positives, $true_negatives, $false_positives, $false_negatives, $akurasi, $error_rate, $ppv, $npv, $sensitivity, $specificity);
		return $array_data_matriks;
	}
	public function get_sentiments_eucledian(){
		$this->db->select('sa_dataset.jenis_sentimen, sa_datauji.sentimen_eucledian');
		$this->db->from('sa_dataset');
		$this->db->where('kategori','DATA UJI');
		$this->db->join('sa_datauji', 'sa_datauji.id_dataset2 = sa_dataset.id_dataset');
		$array_all_sentiments = $this->db->get()->result_array();
		return $array_all_sentiments;
	}
	public function matrix_akurasi_eucledian(){
		$array_sentiments = $this->get_sentiments_eucledian();
		$total_datauji =  count($array_sentiments);
		$true_positives =0;
		$true_negatives =0;
		$false_positives =0;
		$false_negatives =0;
		
		foreach($array_sentiments as $sentiment){
			if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_eucledian"]=="POSITIF"){
				$true_positives = $true_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_eucledian"]=="NEGATIF"){
				$true_negatives = $true_negatives+1;
			}
			else if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_eucledian"]=="NEGATIF"){
				$false_positives = $false_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_eucledian"]=="POSITIF"){
				$false_negatives = $false_negatives+1;
			}
		}
		
		$akurasi = ($true_positives+$true_negatives)/$total_datauji; //AKURASI :(true positives+true negatives)/total data uji
		$ppv = $true_positives/($true_positives+$false_positives); //POSITIVE PREDICTION VALUE /PRESISI : true positives/(true positives+false positives)
		$sensitivity = $true_positives/($true_positives+$false_negatives); //SENSITIVITY /RECALL : true positives/(true positives+false negatives)
		$array_data_matriks = array($total_datauji, $true_positives, $true_negatives, $false_positives, $false_negatives, $akurasi, $ppv, $sensitivity);
		return $array_data_matriks;
	}
	public function get_sentiments_cosine(){
		$this->db->select('sa_dataset.jenis_sentimen, sa_datauji.sentimen_cosine');
		$this->db->from('sa_dataset');
		$this->db->where('kategori','DATA UJI');
		$this->db->join('sa_datauji', 'sa_datauji.id_dataset2 = sa_dataset.id_dataset');
		$array_all_sentiments = $this->db->get()->result_array();
		return $array_all_sentiments;
	}
	public function matrix_akurasi_cosine(){
		$array_sentiments = $this->get_sentiments_cosine();
		$total_datauji =  count($array_sentiments);
		$true_positives =0;
		$true_negatives =0;
		$false_positives =0;
		$false_negatives =0;
		
		foreach($array_sentiments as $sentiment){
			if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_cosine"]=="POSITIF"){
				$true_positives = $true_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_cosine"]=="NEGATIF"){
				$true_negatives = $true_negatives+1;
			}
			else if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_cosine"]=="NEGATIF"){
				$false_positives = $false_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_cosine"]=="POSITIF"){
				$false_negatives = $false_negatives+1;
			}
		}
		
		$akurasi = ($true_positives+$true_negatives)/$total_datauji; //AKURASI :(true positives+true negatives)/total data uji
		$ppv = $true_positives/($true_positives+$false_positives); //POSITIVE PREDICTION VALUE /PRESISI : true positives/(true positives+false positives)
		$sensitivity = $true_positives/($true_positives+$false_negatives); //SENSITIVITY /RECALL : true positives/(true positives+false negatives)
		$array_data_matriks = array($total_datauji, $true_positives, $true_negatives, $false_positives, $false_negatives, $akurasi, $ppv, $sensitivity);
		return $array_data_matriks;
	}
	public function get_sentiments_jaccord(){
		$this->db->select('sa_dataset.jenis_sentimen, sa_datauji.sentimen_jaccord');
		$this->db->from('sa_dataset');
		$this->db->where('kategori','DATA UJI');
		$this->db->join('sa_datauji', 'sa_datauji.id_dataset2 = sa_dataset.id_dataset');
		$array_all_sentiments = $this->db->get()->result_array();
		return $array_all_sentiments;
	}
	public function matrix_akurasi_jaccord(){
		$array_sentiments = $this->get_sentiments_jaccord();
		$total_datauji =  count($array_sentiments);
		$true_positives =0;
		$true_negatives =0;
		$false_positives =0;
		$false_negatives =0;
		
		foreach($array_sentiments as $sentiment){
			if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_jaccord"]=="POSITIF"){
				$true_positives = $true_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_jaccord"]=="NEGATIF"){
				$true_negatives = $true_negatives+1;
			}
			else if($sentiment["jenis_sentimen"]=="POSITIF" && $sentiment["sentimen_jaccord"]=="NEGATIF"){
				$false_positives = $false_positives+1;
			}
			else if($sentiment["jenis_sentimen"]=="NEGATIF" && $sentiment["sentimen_jaccord"]=="POSITIF"){
				$false_negatives = $false_negatives+1;
			}
		}
		
		$akurasi = ($true_positives+$true_negatives)/$total_datauji; //AKURASI :(true positives+true negatives)/total data uji
		$ppv = $true_positives/($true_positives+$false_positives); //POSITIVE PREDICTION VALUE /PRESISI : true positives/(true positives+false positives)
		$sensitivity = $true_positives/($true_positives+$false_negatives); //SENSITIVITY /RECALL : true positives/(true positives+false negatives)
		$array_data_matriks = array($total_datauji, $true_positives, $true_negatives, $false_positives, $false_negatives, $akurasi, $ppv, $sensitivity);
		return $array_data_matriks;
	}
	public function get_all_data_latih(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA LATIH');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
	}
	public function get_label_latih(){
		$this->db->select('jenis_sentimen');
		$this->db->from('sa_dataset');
		$this->db->where('kategori','DATA LATIH');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
	}
	public function get_all_data_uji(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA UJI');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
	}
	public function get_all_id_data_uji(){
		$this->db->select('bag_id_dataset');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA UJI');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
	}
	public function get_term($kalimat){
		$kalimat = implode(" ",$kalimat);
		$kalimat = preg_replace('/\s+/', ' ', $kalimat);
		$kalimat = trim($kalimat);
		$kalimat = explode(" ",$kalimat);
		$kalimat_unique = array_unique($kalimat);
		return $kalimat_unique;
	}
	public function get_all_term($kalimat){
		$kalimat = implode(" ",$kalimat);
		$kalimat = preg_replace('/\s+/', ' ', $kalimat);
		$kalimat = trim($kalimat);
		$kalimat = explode(" ",$kalimat);
		return $kalimat;
	}
	public function get_data_uji(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords');
		$this->db->join('sa_dataset', 'sa_dataset.id_dataset = sa_bagofwords.bag_id_dataset');
		$this->db->where('kategori','DATA UJI');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
	}
	public function prediksi($hasil){
		$positif=0;
		$negatif=0;
		$count=count($hasil);
		///MENGHITUNG JUMLAH POSITIF DAN NEGATIF
		for ($i=0; $i < $count; $i++) { 
			if($hasil[$i]["label"]=="POSITIF"){
				$positif++;
			}
			else if($hasil[$i]["label"]=="NEGATIF"){
				$negatif++;
			}
		}

		///MENENTUKAN PREDIKSI
		if($positif>$negatif){
			$prediksi="POSITIF";
		}
		else if($positif<$negatif){
			$prediksi="NEGATIF";
		}
		else if($positif==$negatif){
			$prediksi="NEGATIF";
		}
		else{
			$prediksi="TIDAK DIKETAHUI";

		}
		return $prediksi;
		
	}
	public function tfidficf_latih(){
		$kata = $this->vocabulary();
		$vocab_count=count($kata);
		for ($i=0; $i < $vocab_count; $i++) { 
			$array_term[] = array(
				"term" => $kata[$i],
				"df" => 0,
				"cf" => 0,
				"idf" => 0,
				"icf" => 0
			);
		}
		
		
		
		$kata_datalatih = $this->all_terms_traindata();
		$array_kalimat = $this->get_all_data_latih();
		// HITUNG DF DATA LATIH
		foreach ($array_kalimat as $row2) {
			$row2_unique= $this->get_term($row2);
			for ($i=0; $i < $vocab_count; $i++) {
				foreach($row2_unique as $term2){
					if($array_term[$i]["term"]==$term2){
						$array_term[$i]["df"]++;
					}
				}
			}
		}
		///HITUNG CF DATA LATIH POSITIF
		$array_term_latih_positif = $this->all_terms_traindata_positif();
		foreach ($array_term_latih_positif as $row2) {
			$row2_unique= $this->get_term($row2);
			for ($i=0; $i < $vocab_count; $i++) {
				foreach($row2_unique as $term2){
					if($array_term[$i]["term"]==$term2){
						if($array_term[$i]["cf"]==0){
							$array_term[$i]["cf"]++;
						}
					}
				}
			}
		}
		///HITUNG CF DATA LATIH NEGATIF
		$term_gabungan = array();
		$array_term_latih_negatif = $this->all_terms_traindata_negatif();
		foreach ($array_term_latih_negatif as $row2) {
			$row2 = implode(" ",$row2);
			$row2 = preg_replace('/\s+/', ' ', $row2);
			$row2 = trim($row2);
			$row2 = explode(" ",$row2);
			foreach ($row2 as $term) {
				array_push($term_gabungan,$term);
			}
		}
		$term_gabungan_unique = array_unique($term_gabungan);
		for ($i=0; $i < $vocab_count; $i++) {
			foreach($term_gabungan_unique as $term2){
				if($array_term[$i]["term"]==$term2){
					if($array_term[$i]["cf"]==0||$array_term[$i]["cf"]==1){
						$array_term[$i]["cf"]++;
					}
				}
			}
		}
		
		return $array_term;
	}
	
	public function tfidficf($array_uji,$k,$data_model){
		$array_label_latih=$this->get_label_latih();
		$array_term = $data_model;
		$array_hasil_eucledian=array();
		$array_hasil_cosine=array();
		$array_hasil_jaccord=array();
		$count= count($array_term);
		$array_term_copy = $array_term;
		$array_uji_term = $this->get_all_term($array_uji);
		
		foreach ($array_uji_term as $row2) {
			$array_kata=array();
			$ketemu=0;
			for ($i=0; $i < $count; $i++) { 
				///JIKA TERDAPAT DALAM ARRAY MAKA DF DALAM KATA TERSEBUT BERTAMBAH
				if($row2 == $array_term_copy[$i]["term"]){
					$array_term_copy[$i]["df"]++;
					$ketemu++;
				}
			}
			///JIKA TIDAK TERDAPAT PADA ARRAY MAKA AKAN MENAMBAHKAN KATA-KATA BARU DALAM ARRAY
			if($ketemu==0){
				array_push($array_term_copy,array(
					"term" => $row2,
					"df" => 1,
					"cf" => 0,
					"idf" => 0,
					"icf" => 0
				));
			}
			$count= count($array_term_copy); 
			
		}

		///MEGHITUNG IDF ICF
		for ($i=0; $i < $count; $i++) {
			$array_term_copy[$i]["idf"]=log10(1+(5/$array_term_copy[$i]["df"]));
			$array_term_copy[$i]["icf"]=log10(1+(1+2)/(1+$array_term_copy[$i]["cf"]));
		}
		///COPY ARRAY
		$count= count($array_term_copy); 
		for($i=0; $i< $count ;$i++){
			$array_kata[] = array(
				"term" => $array_term_copy[$i]["term"],
				"tf" => 0,
				"tfidficf" => 0,
				"tfidficf_pangkat" =>0
			);
		}
		
		//AMBIL SEMUA KALIMAT LATIH
		$array_kalimat_latih = $this->get_all_data_latih();
		$array_data_latih=$array_kata;
		$array_data_uji=$array_kata;
		///MENGHITUNG TF DALAM KALIMAT DATA UJI
		for ($i=0; $i < $count; $i++) { 
			foreach ($array_uji_term as $row2) {
				if($array_data_uji[$i]["term"]==$row2){
					$array_data_uji[$i]["tf"]++;
				}
			}
			
		}
		
		foreach ($array_kalimat_latih as $kalimat) {
			$array_latih_term = $this->get_all_term($kalimat);
			$hasil_eucledian=0;
			$hasil_cosine=0;
			$jumlah_pangkat_data_latih = 0;
			$jumlah_pangkat_data_uji = 0;
			$jumlah_perkalian_datauji_x_datalatih=0;
			///MENGHITUNG TF DALAM SETIAP KALIMAT DATA LATIH
			for ($i=0; $i < $count; $i++) { 
				foreach ($array_latih_term as $row2) {
					if($array_data_latih[$i]["term"]==$row2){
						$array_data_latih[$i]["tf"]++;
					}
				}
			}
			
			for ($i=0; $i < $count; $i++) { 
				$hitung_eucledian=0;
				///MENGHITUNG TF IDF ICF
				$array_data_latih[$i]["tfidficf"]=$array_data_latih[$i]["tf"]*$array_term_copy[$i]["idf"]*$array_term_copy[$i]["icf"];
				$array_data_uji[$i]["tfidficf"]=$array_data_uji[$i]["tf"]*$array_term_copy[$i]["idf"]*$array_term_copy[$i]["icf"];
				
				///COSINE SIMILARITY
				$array_data_latih[$i]["tfidficf_pangkat"]=pow($array_data_latih[$i]["tfidficf"],2);
				$array_data_uji[$i]["tfidficf_pangkat"]=pow($array_data_uji[$i]["tfidficf"],2);
				$datauji_x_datatraining = $array_data_latih[$i]["tfidficf"] * $array_data_uji[$i]["tfidficf"];
				$jumlah_perkalian_datauji_x_datalatih=$jumlah_perkalian_datauji_x_datalatih+$datauji_x_datatraining;

				///JACCORD SIMILARITY
				$jumlah_pangkat_data_latih=$jumlah_pangkat_data_latih+$array_data_latih[$i]["tfidficf_pangkat"];
				$jumlah_pangkat_data_uji=$jumlah_pangkat_data_uji+$array_data_uji[$i]["tfidficf_pangkat"];

				///EUCLEDIAN DISTANCE
				$hitung_eucledian=sqrt(pow(($array_data_latih[$i]["tfidficf"]-$array_data_uji[$i]["tfidficf"]),2));
				$hasil_eucledian=$hasil_eucledian+$hitung_eucledian;
			}
			$hasil_jaccord=($jumlah_perkalian_datauji_x_datalatih)/($jumlah_pangkat_data_latih+$jumlah_pangkat_data_uji-$jumlah_perkalian_datauji_x_datalatih);

			$jumlah_pangkat_data_latih=sqrt($jumlah_pangkat_data_latih);
			$jumlah_pangkat_data_uji=sqrt($jumlah_pangkat_data_uji);
			$hasil_cosine=$jumlah_perkalian_datauji_x_datalatih / ($jumlah_pangkat_data_latih * $jumlah_pangkat_data_uji);				

			array_push($array_hasil_eucledian,$hasil_eucledian);
			array_push($array_hasil_cosine,$hasil_cosine);
			array_push($array_hasil_jaccord,$hasil_jaccord);

			$array_data_latih=$array_kata;
		}

		///SORTING DARI TERBESAR KE TERKECIL
		arsort($array_hasil_eucledian);
		arsort($array_hasil_cosine);
		arsort($array_hasil_jaccord);

		///MENGAMBIL ARRAY SEBANYAK K
		$hasil_knn_cosine=array_slice($array_hasil_cosine,0,$k,true);
		$hasil_knn_eucledian=array_slice($array_hasil_eucledian,0,$k,true);
		$hasil_knn_jaccord=array_slice($array_hasil_jaccord,0,$k,true);
		
		foreach ($hasil_knn_eucledian as $key => $value) {
			if($value!=0){
				$hasil_eucledian_knn[]=array(
					"eucledian" => $value,
					"label" => $array_label_latih[$key]["jenis_sentimen"]
				);
			}
			else{
				$hasil_eucledian_knn[]=array(
					"eucledian" => $value,
					"label" => NULL
				);
			}
			
		}
		$prediksi_eucledian = $this->prediksi($hasil_eucledian_knn);

		foreach ($hasil_knn_cosine as $key => $value) {
			if($value!=0){
				$hasil_cosine_knn[]=array(
					"eucledian" => $value,
					"label" => $array_label_latih[$key]["jenis_sentimen"]
				);
			}
			else{
				$hasil_cosine_knn[]=array(
					"eucledian" => $value,
					"label" => NULL
				);
			}
			
		}
		$prediksi_cosine = $this->prediksi($hasil_cosine_knn);

		foreach ($hasil_knn_jaccord as $key => $value) {
			if($value!=0){
				$hasil_jaccord_knn[]=array(
					"eucledian" => $value,
					"label" => $array_label_latih[$key]["jenis_sentimen"]
				);
			}
			else{
				$hasil_jaccord_knn[]=array(
					"eucledian" => $value,
					"label" => NULL
				);
			}
			
		}
		$prediksi_jaccord = $this->prediksi($hasil_jaccord_knn);
		
		$hasil_prediksi[]=array(
			"sentimen_eucledian" => $prediksi_eucledian,
			"sentimen_cosine" => $prediksi_cosine,
			"sentimen_jaccord" => $prediksi_jaccord
		);
		
		
		return $hasil_prediksi;
	}
	public function tfidf($array_uji,$k){
		$array_label_latih=$this->get_label_latih();
		$array_term = $this->tfidficf_latih();
		$array_hasil_eucledian=array();
		$array_hasil_cosine=array();
		$array_hasil_jaccord=array();
		$count= count($array_term);
		$array_term_copy = $array_term;
		$array_uji_term = $this->get_all_term($array_uji);

		foreach ($array_uji_term as $row2) {
			$array_kata=array();
			$ketemu=0;
			for ($i=0; $i < $count; $i++) { 
				///JIKA TERDAPAT DALAM ARRAY MAKA DF DALAM KATA TERSEBUT BERTAMBAH
				if($row2 == $array_term_copy[$i]["term"]){
					$array_term_copy[$i]["df"]++;
					$ketemu++;
				}
			}
			///JIKA TIDAK TERDAPAT PADA ARRAY MAKA AKAN MENAMBAHKAN KATA-KATA BARU DALAM ARRAY
			if($ketemu==0){
				array_push($array_term_copy,array(
					"term" => $row2,
					"df" => 1,
					"cf" => 0,
					"idf" => 0,
					"icf" => 0
				));
			}
			$count= count($array_term_copy); 
			
		}

		///MEGHITUNG IDF ICF
		for ($i=0; $i < $count; $i++) {
			$array_term_copy[$i]["idf"]=log10(1+(5/$array_term_copy[$i]["df"]));
			$array_term_copy[$i]["icf"]=log10(1+(1+2)/(1+$array_term_copy[$i]["cf"]));
		}
		///COPY ARRAY
		$count= count($array_term_copy); 
		for($i=0; $i< $count ;$i++){
			$array_kata[] = array(
				"term" => $array_term_copy[$i]["term"],
				"tf" => 0,
				"tfidficf" => 0,
				"tfidficf_pangkat" =>0
			);
		}
		
		//AMBIL SEMUA KALIMAT LATIH
		$array_kalimat_latih = $this->get_all_data_latih();
		$array_data_latih=$array_kata;
		$array_data_uji=$array_kata;
		///MENGHITUNG TF DALAM KALIMAT DATA UJI
		for ($i=0; $i < $count; $i++) { 
			foreach ($array_uji_term as $row2) {
				if($array_data_uji[$i]["term"]==$row2){
					$array_data_uji[$i]["tf"]++;
				}
			}
			
		}
		
		foreach ($array_kalimat_latih as $kalimat) {
			$array_latih_term = $this->get_all_term($kalimat);
			$hasil_eucledian=0;
			$hasil_cosine=0;
			$jumlah_pangkat_data_latih = 0;
			$jumlah_pangkat_data_uji = 0;
			$jumlah_perkalian_datauji_x_datalatih=0;
			///MENGHITUNG TF DALAM SETIAP KALIMAT DATA LATIH
			for ($i=0; $i < $count; $i++) { 
				foreach ($array_latih_term as $row2) {
					if($array_data_latih[$i]["term"]==$row2){
						$array_data_latih[$i]["tf"]++;
					}
				}
			}
			
			for ($i=0; $i < $count; $i++) { 
				$hitung_eucledian=0;
				///MENGHITUNG TF IDF
				$array_data_latih[$i]["tfidficf"]=$array_data_latih[$i]["tf"]*$array_term_copy[$i]["idf"];
				$array_data_uji[$i]["tfidficf"]=$array_data_uji[$i]["tf"]*$array_term_copy[$i]["idf"];
				
				///COSINE SIMILARITY
				$array_data_latih[$i]["tfidficf_pangkat"]=pow($array_data_latih[$i]["tfidficf"],2);
				$array_data_uji[$i]["tfidficf_pangkat"]=pow($array_data_uji[$i]["tfidficf"],2);
				$datauji_x_datatraining = $array_data_latih[$i]["tfidficf"] * $array_data_uji[$i]["tfidficf"];
				$jumlah_perkalian_datauji_x_datalatih=$jumlah_perkalian_datauji_x_datalatih+$datauji_x_datatraining;

				///JACCORD SIMILARITY
				$jumlah_pangkat_data_latih=$jumlah_pangkat_data_latih+$array_data_latih[$i]["tfidficf_pangkat"];
				$jumlah_pangkat_data_uji=$jumlah_pangkat_data_uji+$array_data_uji[$i]["tfidficf_pangkat"];

				///EUCLEDIAN DISTANCE
				$hitung_eucledian=sqrt(pow(($array_data_latih[$i]["tfidficf"]-$array_data_uji[$i]["tfidficf"]),2));
				$hasil_eucledian=$hasil_eucledian+$hitung_eucledian;
			}
			$hasil_jaccord=($jumlah_perkalian_datauji_x_datalatih)/($jumlah_pangkat_data_latih+$jumlah_pangkat_data_uji-$jumlah_perkalian_datauji_x_datalatih);

			$jumlah_pangkat_data_latih=sqrt($jumlah_pangkat_data_latih);
			$jumlah_pangkat_data_uji=sqrt($jumlah_pangkat_data_uji);
			$hasil_cosine=$jumlah_perkalian_datauji_x_datalatih / ($jumlah_pangkat_data_latih * $jumlah_pangkat_data_uji);				

			array_push($array_hasil_eucledian,$hasil_eucledian);
			array_push($array_hasil_cosine,$hasil_cosine);
			array_push($array_hasil_jaccord,$hasil_jaccord);

			$array_data_latih=$array_kata;
		}

		///SORTING DARI TERBESAR KE TERKECIL
		arsort($array_hasil_eucledian);
		arsort($array_hasil_cosine);
		arsort($array_hasil_jaccord);

		///MENGAMBIL ARRAY SEBANYAK K
		$hasil_knn_cosine=array_slice($array_hasil_cosine,0,$k,true);
		$hasil_knn_eucledian=array_slice($array_hasil_eucledian,0,$k,true);
		$hasil_knn_jaccord=array_slice($array_hasil_jaccord,0,$k,true);
		
		foreach ($hasil_knn_eucledian as $key => $value) {
			if($value!=0){
				$hasil_eucledian_knn[]=array(
					"eucledian" => $value,
					"label" => $array_label_latih[$key]["jenis_sentimen"]
				);
			}
			else{
				$hasil_eucledian_knn[]=array(
					"eucledian" => $value,
					"label" => NULL
				);
			}
			
		}
		$prediksi_eucledian = $this->prediksi($hasil_eucledian_knn);

		foreach ($hasil_knn_cosine as $key => $value) {
			if($value!=0){
				$hasil_cosine_knn[]=array(
					"eucledian" => $value,
					"label" => $array_label_latih[$key]["jenis_sentimen"]
				);
			}
			else{
				$hasil_cosine_knn[]=array(
					"eucledian" => $value,
					"label" => NULL
				);
			}
			
		}
		$prediksi_cosine = $this->prediksi($hasil_cosine_knn);

		foreach ($hasil_knn_jaccord as $key => $value) {
			if($value!=0){
				$hasil_jaccord_knn[]=array(
					"eucledian" => $value,
					"label" => $array_label_latih[$key]["jenis_sentimen"]
				);
			}
			else{
				$hasil_jaccord_knn[]=array(
					"eucledian" => $value,
					"label" => NULL
				);
			}
			
		}
		$prediksi_jaccord = $this->prediksi($hasil_jaccord_knn);
		
		$hasil_prediksi[]=array(
			"sentimen_eucledian" => $prediksi_eucledian,
			"sentimen_cosine" => $prediksi_cosine,
			"sentimen_jaccord" => $prediksi_jaccord
		);
		
		
		return $hasil_prediksi;
	}
}
