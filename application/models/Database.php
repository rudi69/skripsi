<?php
	class Database extends CI_Model{
        function __construct(){
            parent::__construct();
            $this->load->database();
        }
        function create($tabel,$create){
            $this->db->insert($tabel, $create);
            return $this->db->insert_id();
        }
        function select2($kolom,$tabel,$orderby=''){
            $this->db->select($kolom);
            $this->db->from($tabel);
            $this->db->order_by($orderby, "asc");
            return $this->db->get()->result_array();
        }
        function select($kolom,$tabel,$kondisi,$orderby=''){
            $this->db->select($kolom);
            $this->db->from($tabel);
            $this->db->where($kondisi);
            $this->db->order_by($orderby, "asc");
            return $this->db->get()->result_array();
        }
        function select3($kolom,$tabel,$kondisi,$kondisi2,$orderby=''){
            $this->db->select($kolom);
            $this->db->from($tabel);
            $this->db->where($kondisi);
            $this->db->where($kondisi2);
            $this->db->order_by($orderby, "asc");
            return $this->db->get()->result_array();
        }
        function session(){
            $this->db->select("*");
            $this->db->from("tmp_session");
            $this->db->join("hi_mahasiswa",'ses_idmhs = mhs_idmhs','left');
            $this->db->join("hi_komputer",'ses_idkmp = kmp_idkmp','left');
            $this->db->order_by('kmp_nomor',"asc");
            return $this->db->get()->result_array();
        }
        function delete($tabel,$kondisi){
            $this->db->delete($tabel,$kondisi);
            return $this->db->affected_rows();
        }
        function update($tabel,$kondisi,$data){
            $this->db->where($kondisi);
            $this->db->update($tabel, $data);
            return $this->db->affected_rows();
        }
        function login($tabel,$prefix,$username,$password){
            $this->db->select('*');
            $this->db->from($tabel);
            $this->db->where("'$username' = `".$prefix."_name` AND '".sha1(sha1($password))."' = `".$prefix."_password`");
            return $this->db->get()->result_array();
        }
    }
?>
