<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Beranda
      </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Beranda</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="jumlah-data-latih">
              <?php
                  $a = $this->database->select("*","sa_dataset","kategori='DATA LATIH'");
                  echo count($a); 
                ?>
              </h3>

              <p>Data Latih</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="<?php echo base_url('admin_dataset') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="jumlah-data-latih-positif">
              <?php
                  $a = $this->database->select("*","sa_dataset","jenis_sentimen='POSITIF' AND kategori='DATA LATIH'");
                  echo count($a); 
                ?>
              </h3>

              <p>Data Latih Positif</p>
            </div>
            <div class="icon">
              <i class="fa  fa-thumbs-up"></i>
            </div>
            <a href="<?php echo base_url('admin_dataset') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="jumlah-data-latih-negatif">
              <?php
                  $a = $this->database->select("*","sa_dataset","jenis_sentimen='NEGATIF' AND kategori='DATA LATIH'");
                  echo count($a); 
                ?>
              </h3>

              <p>Data Latih Negatif</p>
            </div>
            <div class="icon">
              <i class="fa  fa-thumbs-down"></i>
            </div>
            <a href="<?php echo base_url('admin_dataset') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="jumlah-data-uji">
              <?php
                  $a = $this->database->select("*","sa_dataset","kategori='DATA UJI'");
                  echo count($a); 
                ?>
              </h3>

              <p>Data Uji</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('admin_dataset') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
      </div>
      <div class="row">
          <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Perbandingan Data Uji dan Latih</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="sales" style="height: 300px; position: relative;"></div>
                </div>
              <!-- /.box-body -->
            </div>
          </div>
          <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Perbandingan Data Uji Positif dan Negatif</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
                </div>
              <!-- /.box-body -->
            </div>
          </div>
      </div>
      
    </section>
    <!-- /.content -->
  </div>