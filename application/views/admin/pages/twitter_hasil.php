<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Hasil Twitter
            <small>Analisis Sentimen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo base_url('admin_twitter') ?>">Hasil</a></li>
            <!-- <li class="active">Data Obat</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php 
				if($this->session->flashdata('message') != null) 
                { 
                    echo '<div class="alert alert-'.$this->session->flashdata('type').'" role="alert" style="margin-inline-start: 12px;margin-inline-end: 12px;">'; 
                    echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'; 
                    echo '<i class="icon fa fa-check"></i>';
                    echo $this->session->flashdata('message') <> '' ? $this->session->flashdata('message') : ''; 
                    echo '</div>'; 
                }
			?>
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            Test
                        </h3>

                        <button type="button" onclick="button_spin_twitter()" class="btn bg-yellow-active"><i class="fa fa-hand-o-right"></i>
                            Klik</button>
                    </div>
                    <div class="icon">
                        <i class="fa fa-book"></i>
                    </div>
                    <a href="<?php echo base_url('admin_twitter_hasil') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 id="jumlah-data-latih">
                            <?php
                                $a = $this->database->select("*","sa_dataset_twitter","id_admin=".$this->session->userdata['ses_admin']['id']);
                                echo count($a); 
                            ?>
                        </h3>

                        <p>Data Twitter</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-twitter"></i>
                    </div>
                    <a href="<?php echo base_url('admin_twitter') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 id="jumlah-data-positif">
                            <?php
                                $a = $this->database->select3("*","sa_dataset_twitter","id_admin=".$this->session->userdata['ses_admin']['id'],"jenis_sentimen='POSITIF'");
                                echo count($a); 
                            ?>
                        </h3>

                        <p>Data Twitter Positif</p>
                    </div>
                    <div class="icon">
                        <i class="fa  fa-thumbs-up"></i>
                    </div>
                    <a href="<?php echo base_url('admin_twitter_hasil_positif') ?>" class="small-box-footer">More info
                        <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 id="jumlah-data-negatif">
                            <?php
                                $a = $this->database->select3("*","sa_dataset_twitter","id_admin=".$this->session->userdata['ses_admin']['id'],"jenis_sentimen='NEGATIF'");
                                echo count($a); 
                            ?>
                        </h3>

                        <p>Data Twitter Negatif</p>
                    </div>
                    <div class="icon">
                        <i class="fa  fa-thumbs-down"></i>
                    </div>
                    <a href="<?php echo base_url('admin_twitter_hasil_negatif') ?>" class="small-box-footer">More info
                        <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Perbandingan Data Twitter Positif dan Negatif</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <canvas id="pieChart" style="height: 265px; width: 530px;" width="530" height="265"></canvas>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>