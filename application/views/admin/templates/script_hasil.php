<script src="<?php echo base_url('assets/admin/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-ui/jquery-ui.min.js');?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/plugins/iCheck/all.css');?>">
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/admin/bower_components/raphael/raphael.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/bower_components/morris.js/morris.min.js');?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js');?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-knob/dist/jquery.knob.min.js');?>"></script>
<!-- daterangepicker --><!-- 
<script src="assets/admin/bower_components/moment/min/moment.min.js"></script>
<script src="assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<!-- datepicker -->
<!-- <script src="assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/admin/bower_components/fastclick/lib/fastclick.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/admin/dist/js/adminlte.min.js');?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="assets/admin/dist/js/pages/dashboard.js"></script>
AdminLTE for demo purposes
<script src="assets/admin/dist/js/demo.js"></script> -->
<!-- DataTables -->
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/swal/js/sweetalert2.all.min.js');?>"></script>
<script type="text/javascript">

var save_method; //for save method string
var table;


$(document).ready(function() {
    //datatables
    table = $('#example1').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url($this->uri->segment(1).'/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": 2,
            render: function ( data, type, row ) {
                return type === 'display' && data.length > 10 ?
                data.substr( 0, 50 ) +'…' :
                data;
            }
        },
        {
            "targets": 3,
            render: function ( data, type, row ) {
                return type === 'display' && data.length > 10 ?
                data.substr( 0, 50 ) +'…' :
                data;
            }
        },
        ],

    });
});
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}
function button_spin(){
    $('#btn').attr('disabled',true); //set button disable
	$.ajax({
        url : "<?php echo base_url($this->uri->segment(1).'/proses')?>",
		data: $('#form_latih').serialize(),
        type: "POST",
        dataType: "JSON",

        beforeSend: function () {
			Swal.fire({
				title: 'Menunggu',
				html: 'Memproses data',
				onOpen: () => {
					swal.showLoading()
				}
			})
		},
        success: function(data)
        {
            
            if(data.status)
            {
                Swal.fire({
				type: 'success',
				title: 'Berhasil',
				text: 'Data berhasil di proses'
			    })
                reload_table();
            }
            $('#btn').attr('disabled',false); //set button enable
			$('#spin').hide();

        }
    });
}
</script>