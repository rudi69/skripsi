<?php 
// if (!$this->session->userdata('ses_user')){
//     redirect(base_url());
// }
?>
<!DOCTYPE html>
<html>
<?php $this->load->view('admin/templates/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <?php $this->load->view('admin/templates/header.php');?>
        <?php $this->load->view('admin/templates/sidebar.php'); ?>
        <?php $this->load->view('admin/pages/'.$page.'.php',$content); ?>  
    </div>
    <?php $this->load->view('admin/templates/script.php'); ?>  

    
</body>
</html>

