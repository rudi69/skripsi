<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_twitter_pre extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "twitter_pre";
        $this->load->view('admin/layout',$data);
    }
    public function selectdata(){
        $hasil = $this->model_twitter_pre->selectdata();
        return $hasil;
    }
    public function proses(){
        $this->db->where('bag_id_admin', $this->session->userdata['ses_admin']['id']);
        $this->db->delete('sa_bagofwords_twitter');
        $select = $this->selectdata();
        //die(var_dump($select));
        $this->model_twitter_pre->insertterm($select);
        // redirect (base_url('admin_twitter_pre'));
        echo json_encode(array("status" => TRUE));


    }
    
    public function ajax_list(){
    	$list=$this->model_twitter_pre->get_datatables();
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $admin_preprocessing) {
    		$no++;
    		$row = array();
            $row[]=$no;
            $row[]=$admin_preprocessing->term_tokenized;
            $row[]=$admin_preprocessing->term_filtered;
            $row[]=$admin_preprocessing->term_stemmed;
            $data[] = $row;
    	}
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_twitter_pre->count_all(),
                        "recordsFiltered" => $this->model_twitter_pre->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
}
