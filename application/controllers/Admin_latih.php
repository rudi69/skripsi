<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_latih extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "latih";
        $this->load->view('admin/layout',$data);
    }
    public function ajax_list(){
    	$list=$this->model_latih->get_datatables();
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $admin_latih) {
    		$no++;
    		$row = array();
            $row[]=$no;
            $row[]=$admin_latih->text;
            $row[]=$admin_latih->sentimen_eucledian;
            $row[]=$admin_latih->sentimen_cosine;
            $row[]=$admin_latih->sentimen_jaccord;
            $data[] = $row;
    	}
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_latih->count_all(),
                        "recordsFiltered" => $this->model_latih->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_edit($id)
    {
        $data = $this->model_latih->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $insert = $this->model_latih->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $this->model_latih->update(array('id_term' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->model_latih->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
    public function insert_train(){
        $ks = $this->input->post('k');
        $k=(int)$ks;
        $this->model_latih->insert_datauji($k);
        $this->session->set_flashdata('message','Berhasil melatih data!');
		$this->session->set_flashdata('type','success');
        redirect (base_url('admin_latih'));
    }
    public function proses(){
        // $ks = $this->input->post('k');
        // $k=(int)$ks;
        $k = $this->input->post('k');
        $this->model_latih->insert_datauji($k);
        $this->session->set_flashdata('message','Berhasil melatih data!');
		$this->session->set_flashdata('type','success');
        //redirect (base_url('admin_latih'));
        echo json_encode(array("status" => TRUE));

	}
}
