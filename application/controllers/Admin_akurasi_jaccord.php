<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_akurasi_jaccord extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "akurasi_jaccord";
        $this->load->view('admin/layout',$data);
    }
    public function ajax_list(){
        $list=$this->model_akurasi->get_datatables();
        //die(var_dump($list));
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $admin_akurasi) {
    		$no++;
    		$row = array();
            $row[]=$no;
            $row[]=$admin_akurasi->text;
            $row[]=$admin_akurasi->jenis_sentimen;
            $row[]=$admin_akurasi->sentimen_jaccord;
            $row[]=$this->model_latih->accuracy_badge($admin_akurasi->jenis_sentimen,$admin_akurasi->sentimen_jaccord);

            $data[] = $row;
        }
        
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_akurasi->count_all(),
                        "recordsFiltered" => $this->model_akurasi->count_filtered(),
                        "data" => $data,
                );
                //die(var_dump($output));
        //output to json format
        echo json_encode($output);
    }
    public function ajax_edit($id)
    {
        $data = $this->model_akurasi->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $insert = $this->model_akurasi->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $this->model_akurasi->update(array('id_datauji' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->model_akurasi->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
    public function insert_datauji(){
        $this->model_latih->insert_datauji();
        echo "berhasil";
    }
    public function matrix_akurasi(){
        $array_data_matrix = $this->model_latih->matrix_akurasi_jaccord();
        //die(var_dump($array_data_matrix));
        echo json_encode($array_data_matrix);
	}
}
