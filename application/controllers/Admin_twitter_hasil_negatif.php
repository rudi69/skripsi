<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_twitter_hasil_negatif extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "twitter_hasil_negatif";
        $this->load->view('admin/layout',$data);
    }
    public function ajax_list(){
        $list=$this->model_twitter_hasil_negatif->get_datatables();
        $data=array();
    	$no= $_POST['start'];
    	foreach ($list as $Admin_twitter) {
    		$no++;
    		$row = array();
            $row[]=$no;
            $row[]=$Admin_twitter->text;
            if($Admin_twitter->jenis_sentimen==NULL){
                $row[]='<span class="badge bg-orange">BELUM DIKETAHUI</span>';
            }else if($Admin_twitter->jenis_sentimen=='NEGATIF'){
                $row[]='<span class="badge bg-red">'.$Admin_twitter->jenis_sentimen.'</span>';
            }
            else{
                $row[]='<span class="badge bg-green">'.$Admin_twitter->jenis_sentimen.'</span>';                
            }
            
            $data[] = $row;
        }
        
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_twitter_hasil_negatif->count_all(),
                        "recordsFiltered" => $this->model_twitter_hasil_negatif->count_filtered(),
                        "data" => $data,
                );
                //die(var_dump($output));
        //output to json format
        echo json_encode($output);
    }
}
