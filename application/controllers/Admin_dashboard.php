<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_dashboard extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        // $data['nama'] = $sess['nama'];
        $data['page']= "dashboard";
        $this->load->view('admin/layout',$data);
    }
}
