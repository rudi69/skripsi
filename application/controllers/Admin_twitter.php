<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_twitter extends CI_Controller
{
    public function __construct()
    {
    parent::__construct();
        $this->load->library('csvimport');
    }
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "twitter";
        $this->load->view('admin/layout',$data);
    }
    public function ajax_list(){
    	$list=$this->model_twitter->get_datatables();
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $Admin_twitter) {
    		$no++;
    		$row = array();
            $row[]=$no;
            $row[]=$Admin_twitter->text;
            if($Admin_twitter->jenis_sentimen==NULL){
                $row[]='<span class="badge bg-orange">BELUM DIKETAHUI</span>';
            }else if($Admin_twitter->jenis_sentimen=='NEGATIF'){
                $row[]='<span class="badge bg-red">'.$Admin_twitter->jenis_sentimen.'</span>';
            }
            else if($Admin_twitter->jenis_sentimen=='POSITIF'){
                $row[]='<span class="badge bg-green">'.$Admin_twitter->jenis_sentimen.'</span>';                
            }else{
                $row[]='<span class="badge bg-orange">'.$Admin_twitter->jenis_sentimen.'</span>';                
            }
            
            $data[] = $row;
    	}
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_twitter->count_all(),
                        "recordsFiltered" => $this->model_twitter->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function delete(){
        $this->model_twitter->delete_all();
        echo json_encode(array("status" => TRUE));
    }
    function import()
	{
        $file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
		foreach($file_data as $row)
		{
			$data[] = array(
                'text'			=>	$row["text"],
                'jenis_sentimen'    => NULL,
                'ID'      => $row["tweet-id"],
                'usernameTweet'      => $row["fullname"],
                'url'      => $row["url"],
                'datetime'      => $row["timestamp"],
                'user_id'      => $row["tweet-id"],
                'id_admin' => $this->session->userdata['ses_admin']['id']
			);
		}
		$this->model_twitter->insert($data);
	} 
}